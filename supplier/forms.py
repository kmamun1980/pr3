from django import forms
from supplier.models import *

class ProfileForm(forms.ModelForm):

    class Meta:
        model = Supplier
        exclude = ['subscription_status', 'profile_completion_status']


class CompInfoForm(forms.ModelForm):
    class Meta:
        model = SupplierCompany
        exclude = ['']


class CapabilityForm(forms.ModelForm):
    class Meta:
        model = CompanyCapabilityStatement
        exclude = ['']


class RevenueForm(forms.ModelForm):
    class Meta:
        model = Revenue
        exclude = ['company',]

class AddressForm(forms.ModelForm):
    class Meta:
        model = Address
        exclude = ['']

class CompanyLicenseForm(forms.ModelForm):
    class Meta:
        model = CompanyLicense
        exclude = ['company',]

class BusinessCertificateForm(forms.ModelForm):
    class Meta:
        model = BusinessCertificate
        exclude = ['company',]
    # def save(self, commit=False):
    #   license = super(CompanyLicenseForm, self).save(commit=False)

class CompanyContactForm(forms.ModelForm):
    class Meta:
        model = CompanyContact
        exclude = ['company',]

class CompanyReferenceForm(forms.ModelForm):
    class Meta:
        model = Reference
        exclude = ['company',]

class CompanyCertificationForm(forms.ModelForm):
    class Meta:
        model = CompanyCertification
        exclude = ['company',]

class CertificateNameForm(forms.ModelForm):
    class Meta:
        model = CertificateName
        exclude = ['display_order','active']

class CompanyProductServiceForm(forms.ModelForm):

    def clean_naics_code(self):
        data = self.cleaned_data['naics_code']
        if data != '':
            return data
        else:
            return data

    def save(self):
        code = super(CompanyProductServiceForm, self).save(commit=False)
        code.save()
        return code
        
    class Meta:
        model = CompanyProductService
        exclude = ['company',]

