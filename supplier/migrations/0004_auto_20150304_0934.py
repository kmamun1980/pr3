# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('supplier', '0003_auto_20150223_1725'),
    ]

    operations = [
        migrations.CreateModel(
            name='CompanyLicense',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('insurance_limit', models.CharField(max_length=50, null=True, blank=True)),
                ('expiration_date', models.DateField(null=True, blank=True)),
                ('insurance_provider', models.CharField(max_length=100, null=True, blank=True)),
                ('business_license', models.FileField(null=True, upload_to=b'/media/license/', blank=True)),
                ('geographical_service_area', models.CharField(max_length=20, null=True, blank=True)),
                ('has_online_catalog', models.NullBooleanField()),
                ('can_sell_online', models.NullBooleanField()),
                ('accepts_credit_card', models.NullBooleanField()),
                ('is_edi_capable', models.NullBooleanField()),
                ('company', models.ForeignKey(to='supplier.SupplierCompany')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='companyrevenue',
            name='company',
        ),
        migrations.RemoveField(
            model_name='companyrevenue',
            name='payment_address',
        ),
        migrations.DeleteModel(
            name='CompanyRevenue',
        ),
    ]
