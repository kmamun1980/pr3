# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('supplier', '0010_auto_20150305_0635'),
    ]

    operations = [
        migrations.RenameField(
            model_name='companylicense',
            old_name='is_edi_capable',
            new_name='is_edit_capable',
        ),
    ]
