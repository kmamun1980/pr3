# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('supplier', '0007_auto_20150304_1902'),
    ]

    operations = [
        migrations.RenameField(
            model_name='companycontact',
            old_name='email',
            new_name='contact_email',
        ),
        migrations.RenameField(
            model_name='companycontact',
            old_name='fax',
            new_name='contact_fax',
        ),
        migrations.RenameField(
            model_name='companycontact',
            old_name='name',
            new_name='contact_name',
        ),
        migrations.RenameField(
            model_name='companycontact',
            old_name='phone',
            new_name='contact_phone',
        ),
        migrations.RenameField(
            model_name='companycontact',
            old_name='phone_ext',
            new_name='contact_phone_ext',
        ),
        migrations.RenameField(
            model_name='companycontact',
            old_name='title',
            new_name='contact_title',
        ),
        migrations.RenameField(
            model_name='reference',
            old_name='company_name',
            new_name='ref_company_name',
        ),
        migrations.RenameField(
            model_name='reference',
            old_name='contact_name',
            new_name='ref_contact_name',
        ),
        migrations.RenameField(
            model_name='reference',
            old_name='email',
            new_name='ref_email',
        ),
        migrations.RenameField(
            model_name='reference',
            old_name='phone',
            new_name='ref_phone',
        ),
        migrations.RenameField(
            model_name='reference',
            old_name='services_provided',
            new_name='ref_services_provided',
        ),
    ]
