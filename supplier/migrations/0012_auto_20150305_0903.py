# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('supplier', '0011_auto_20150305_0900'),
    ]

    operations = [
        migrations.AlterField(
            model_name='companylicense',
            name='geographical_service_area',
            field=models.CharField(max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
    ]
