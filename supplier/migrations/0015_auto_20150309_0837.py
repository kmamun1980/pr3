# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('supplier', '0014_auto_20150309_0806'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='companycertification',
            unique_together=set([('certificate_name', 'company')]),
        ),
    ]
