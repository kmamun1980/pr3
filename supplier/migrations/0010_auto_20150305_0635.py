# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('supplier', '0009_auto_20150305_0632'),
    ]

    operations = [
        migrations.AlterField(
            model_name='companylicense',
            name='accepts_credit_card',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='companylicense',
            name='can_sell_online',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='companylicense',
            name='has_online_catalog',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='companylicense',
            name='is_edi_capable',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
