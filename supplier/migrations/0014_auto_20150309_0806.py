# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('supplier', '0013_auto_20150305_1116'),
    ]

    operations = [
        migrations.AlterField(
            model_name='businesscertificate',
            name='company',
            field=models.ForeignKey(related_name='comp_bus_cert', to='supplier.SupplierCompany'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='companycertification',
            name='certificate_name',
            field=models.ForeignKey(related_name='company_cert', to='supplier.CertificateName'),
            preserve_default=True,
        ),
    ]
