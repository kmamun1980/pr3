# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('supplier', '0012_auto_20150305_0903'),
    ]

    operations = [
        migrations.AlterField(
            model_name='businesscertificate',
            name='upload_file',
            field=models.FileField(null=True, upload_to=b'certificate/', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='companylicense',
            name='business_license',
            field=models.FileField(null=True, upload_to=b'license/', blank=True),
            preserve_default=True,
        ),
    ]
