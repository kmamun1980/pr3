# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('supplier', '0006_auto_20150304_1226'),
    ]

    operations = [
        migrations.AlterField(
            model_name='companycontact',
            name='company',
            field=models.OneToOneField(to='supplier.SupplierCompany'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='reference',
            name='company',
            field=models.OneToOneField(to='supplier.SupplierCompany'),
            preserve_default=True,
        ),
    ]
