# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('supplier', '0004_auto_20150304_0934'),
    ]

    operations = [
        migrations.CreateModel(
            name='CompanyRevenue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('last_year_annual_sale', models.CharField(max_length=10, null=True, blank=True)),
                ('two_year_before_annual_sale', models.CharField(max_length=10, null=True, blank=True)),
                ('three_year_before_annual_sale', models.CharField(max_length=10, null=True, blank=True)),
                ('is_company_diverse', models.BooleanField(default=False)),
                ('is_company_publicly_traded', models.BooleanField(default=False)),
                ('is_sba', models.BooleanField(default=False)),
                ('minority_owned', models.BooleanField(default=False)),
                ('veteron_owned', models.BooleanField(default=False)),
                ('us_citizen_owned', models.BooleanField(default=False)),
                ('woman_owned', models.BooleanField(default=False)),
                ('service_disabled_vet', models.BooleanField(default=False)),
                ('owners_ethnicities', models.CharField(max_length=5, null=True, blank=True)),
                ('remit_to_email', models.CharField(max_length=50, null=True, blank=True)),
                ('company', models.OneToOneField(to='supplier.SupplierCompany')),
                ('payment_address', models.ForeignKey(blank=True, to='supplier.Address', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
