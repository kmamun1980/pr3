# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('supplier', '0008_auto_20150305_0614'),
    ]

    operations = [
        migrations.AlterField(
            model_name='companylicense',
            name='company',
            field=models.OneToOneField(to='supplier.SupplierCompany'),
            preserve_default=True,
        ),
    ]
