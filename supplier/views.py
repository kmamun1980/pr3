from django.shortcuts import render
from django.contrib import auth, messages
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from subscriber.models import *
from supplier.models import *
from supplier.forms import *
from supplier.choices import * 

from django.core import serializers
import json
from django.utils.timezone import utc
import datetime
# from supplier.models import Supplier
from haystack.forms import ModelSearchForm, SearchForm
from haystack.query import SearchQuerySet, EmptySearchQuerySet
from haystack.inputs import Raw, Clean, AutoQuery

from supplier.serializer import *


def search_buyer(request):
    query = request.GET.get('q','')
    if request.GET.get('q'):
        buyers = SearchQuerySet().using('default').filter(content=AutoQuery(query)).load_all()[:10]
    else:
        buyers = SearchQuerySet().using('default').all().load_all()[:10]

    buyer_list = []
    if buyers:
        for buyer in buyers:
            if buyer != None:
                buyer_dict = {}
                buyer_dict['id'] = buyer.pk
                buyer_dict['company_name'] = buyer.company_name
                
                buyer_list.append(buyer_dict)

    the_data = json.dumps({
        'results': buyer_list
    })
    return HttpResponse(the_data, content_type='application/json')


# @login_required
def search_corporation(request):
    saved_buyers = []
    ps_categories = ProductAndServiceCategory.objects.all()
    sp = Supplier.objects.get(user=request.user)
    # ssubmissions = sp.suppliersubmission_set.all()
    ssb = SupplierSelectedBuyers.objects.filter(supplier=sp)

    if ssb:
        for ssubmission in ssb:
            saved_buyers.append(ssubmission.buyer)

    return render(request, "supplier/search_corporation.html", 
        {'categories': ps_categories, 'saved_buyers': saved_buyers})


@csrf_exempt
def search_buyers(request):
    saved_buyers = []
    buyers_list = []
    final_buyers_list = []
    sp = Supplier.objects.get(user=request.user)
    # ssubmissions = sp.suppliersubmission_set.all()
    ssb = SupplierSelectedBuyers.objects.filter(supplier=sp)

    if ssb:
        for ssubmission in ssb:
            saved_buyers.append(ssubmission.buyer)

    showall = request.POST['showall']

    if len(request.POST['findbuyer']) > 0:
        findbuyers = request.POST['findbuyer'].split(", ")
    else:
        findbuyers = None

    if len(request.POST['findcat']) > 0:
        findcats = request.POST['findcat'].split(", ")
    else:
        findcats = None

    #====== filtering buyers for categories ===========

    clist = []    
    if findcats:
        for cat in findcats:
            catq1 = ProductAndServiceCategory.objects.filter(category_name__contains=cat).distinct()
            for cq in catq1:
                clist.append(cq.pk)

    if len(request.POST['categories']) > 0:
        categories = request.POST['categories']

        cats = categories.split(", ")

        for cat in cats:
            #cid = cat.split("=")[1]
            clist.append(cat)
    
    buyers_from_cat = Buyer.objects.filter(product_service_category__in=clist).distinct()
    for br in buyers_from_cat:
        buyers_list.append(br)

    #====== filtering buyers for manual  input ===========
    no_buyer_found = []

    if findbuyers:
        for findbuyer in findbuyers:
            try:
                fbuyers = Buyer.objects.filter(company_name__contains=findbuyer).distinct()
                for fb in fbuyers:
                    if fb not in buyers_list:
                        buyers_list.append(fb)
            except (ValueError, Buyer.DoesNotExist):
                no_buyer_found.append(findbuyer)
    

    if showall == 'no':

        
        for bl in buyers_list:
            if bl not in saved_buyers:
                final_buyers_list.append(bl)
    else:
        final_buyers_list = buyers_list

    total_suppliers = Supplier.objects.count()

    return render(request, 'supplier/searched_buyers.html',
        {'buyers': final_buyers_list, 'saved_buyers':saved_buyers, 'no_buyer_found':no_buyer_found, "total_suppliers":total_suppliers })

@csrf_exempt
def save_buyers(request):
    # import pdb; pdb.set_trace()
    if len(request.POST['selectedbuyers']) > 0:

        #===== get all selected buyers============
        buyer_list = []
        buyers = request.POST['selectedbuyers']
        buyerslist = buyers.split("&")[1:]
        for br in buyerslist:
            bid = br.split("=")[1]
            buyerobj = Buyer.objects.get(pk=bid)
            buyer_list.append(buyerobj)

        #========= get supplier ===================
        
        sp = Supplier.objects.get(user=request.user)

        #========= get saved supplier if any ====== 
        saved_buyers = []        
        ssbs = SupplierSelectedBuyers.objects.filter(supplier=sp)
        if ssbs:
            for ssubmission in ssbs:
                saved_buyers.append(ssubmission.buyer)
        
        
        duplicate_buyers = []
        added_buyers = []
        sdate = datetime.datetime.utcnow().replace(tzinfo=utc)

        for buyer in buyer_list:
            
            if buyer not in saved_buyers:
                ssb = SupplierSelectedBuyers(supplier=sp, buyer=buyer)
                ssb.submission_status = 1
                ssb.save()
                added_buyers.append(buyer)
            else:
                duplicate_buyers.append(buyer)
        
        saved_buyers = saved_buyers + added_buyers
        return render(request, 'supplier/selected_buyers.html',
                {'saved_buyers': saved_buyers, 'duplicate_buyers': duplicate_buyers})
        
    else:
        return HttpResponse('No')

@csrf_exempt
def delete_buyer(request):
    bid = request.POST['bid']
    buyer = Buyer.objects.get(pk=bid)
    usr = request.user
    sp = Supplier.objects.get(user=usr)
    ssb = SupplierSelectedBuyers.objects.get(supplier=sp, buyer=buyer)
    ssb.delete()
    return HttpResponse(bid)
    # saved_buyers = []        
    # ssubmissions = SupplierSelectedBuyers.objects.filter(supplier=sp, buyer=buyer)
    # if ssubmissions:
    #     for ssubmission in ssubmissions:
    #         saved_buyers.append(ssubmission.buyer)

    return render(request, 'supplier/selected_buyers.html',
                {'saved_buyers': saved_buyers})


def add_manual_buyer(request):
    pass

# @login_required
def supplier_profile(request):
    salutations = SALUTATION
    user = request.user
    supplier, created = Supplier.objects.get_or_create(user=user)
    print (created)
    print (supplier.profile_completion_status)
    if created:
        supplier.profile_completion_status = -1
        supplier.save()
    if request.method == 'POST':
        profile_form = ProfileForm(request.POST, instance=supplier)
        if profile_form.is_valid():
            sup = profile_form.save()
            if int(sup.profile_completion_status) < 0:
                sup.profile_completion_status = 0
                sup.save()
            redirect_url = sup.get_profile_completion_status_redirect_url()
            return HttpResponseRedirect(redirect_url)
        else:
            return HttpResponseRedirect("/supplier/profile/info/")
    else:
        
        return render(request, "supplier/supplier_profile.html", 
            {'user': user, 'supplier': supplier, 'salutations': salutations})

def supplier_profile_json(request, id):
    supplier = Supplier.objects.get(id=id)
    data = serializers.serialize('json', [supplier,])
    struct = json.loads(data)
    data = json.dumps(struct[0])
    return HttpResponse(data, content_type='application/json')


# @login_required
def comapny_info(request):
    '''
    # Geting user and supplier logged in
    # Check if this supplier already complated the supplier info status = 0
    # Getting or creating SupplierCompany
    # 
    '''
    user = request.user
    sup = Supplier.objects.get(user=user)
    ps_categories = ProductAndServiceCategory.objects.all()

    comp, created = SupplierCompany.objects.get_or_create(supplier=sup)
    if created:
        capability = CompanyCapabilityStatement.objects.create()
        comp.capability = capability
        comp.save()
    else:
        capability = comp.capability
        if not capability:
            capability = CompanyCapabilityStatement.objects.create()
            comp.capability = capability

    selected_categories = comp.ps_categories.all()

    if sup.profile_completion_status >= 0:
        if request.method == 'POST':

            comp_form = CompInfoForm(request.POST, instance=comp)
            capability_form = CapabilityForm(request.POST, instance=capability)
            
            if comp_form.is_valid() and capability_form.is_valid():
                comp = comp_form.save()
                capability = capability_form.save()

                sup.profile_completion_status = 1
                sup.save()
                redirect_url = sup.get_profile_completion_status_redirect_url()
                return HttpResponseRedirect(redirect_url)
                
        else:
            comp_form = CompInfoForm()
            capability_form = CapabilityForm()

            return render(request, "supplier/comapny_info.html", 
                {'capability_form': capability_form, 'user': user, 'supplier': sup, 
                'ps_categories': ps_categories, 'selected_categories': selected_categories})
    else:
        messages.warning(request, 'You must fill supplier info before supplier company info')
        return HttpResponseRedirect("/supplier/profile/info/")

    

# @login_required
def comapny_info_json(request, supplier_id):
    
    supplier = Supplier.objects.get(id=supplier_id)
    supplier_company = SupplierCompany.objects.get(supplier=supplier)
    serializer = CompanySerializer(supplier_company)
    data = json.dumps(serializer.data)
    return HttpResponse(data, content_type='application/json')


def company_revenue(request):
    # import pdb; pdb.set_trace();
    user = request.user
    supplier = Supplier.objects.get(user=user)
    company = SupplierCompany.objects.get(supplier=supplier)
    try:
        revenue = Revenue.objects.get(company=company)
        pay_address = revenue.payment_address
    except Exception, e:
        revenue = Revenue.objects.create(company=company)
        pay_address = Address.objects.create(address_title='Pay Address')
        revenue.payment_address = pay_address
        revenue.save()
    
    if request.method == 'POST':
        revenue_form = RevenueForm(request.POST, instance=revenue)        
        address_form = AddressForm(request.POST, instance=pay_address)

        if revenue_form.is_valid() and address_form.is_valid():
            comp_rev = revenue_form.save()
            pay_address = address_form.save()
            comp_rev.payment_address = pay_address
            comp_rev.save()
            
            if int(supplier.profile_completion_status) == 1:
                supplier.profile_completion_status = 2
                supplier.save()
            
            redirect_url = supplier.get_profile_completion_status_redirect_url()
            return HttpResponseRedirect(redirect_url)
        else:
            return render(request, "supplier/company_revenue.html", 
                {'sales': ANNUAL_SALE, 'revenue_form': revenue_form, 'revenue': revenue, 'supplier': supplier,
                'company': company, 'user': user})
    else:
        revenue_form = RevenueForm()


        return render(request, "supplier/company_revenue.html", 
        {'sales': ANNUAL_SALE, 'revenue_form': revenue_form, 'revenue': revenue, 
        'supplier': supplier, 'company': company, 'user': user})

# @login_required
def company_revenue_json(request, supplier_id):
    supplier = Supplier.objects.get(id=supplier_id)
    supplier_company= SupplierCompany.objects.get(supplier=supplier)
    comp_revenue = Revenue.objects.get(company=supplier_company)
    serializer = RevenueSerializer(comp_revenue)
    data = json.dumps(serializer.data)
    return HttpResponse(data, content_type='application/json')



def company_contacts(request):
    user = request.user
    supplier = Supplier.objects.get(user=user)
    company = SupplierCompany.objects.get(supplier=supplier)
    try:
        reference = Reference.objects.get(company=company)
    except Exception, e:
        reference = Reference.objects.create(company=company)
    
    try:
        company_contact = CompanyContact.objects.get(company=company)
    except Exception, e:
        company_contact = CompanyContact.objects.create(company=company)

    if request.method == "POST":
        comp_contact_form = CompanyContactForm(request.POST, instance=company_contact)
        comp_reference_form = CompanyReferenceForm(request.POST, instance=reference)

        if comp_contact_form.is_valid() and comp_reference_form.is_valid():
            comp_contact = comp_contact_form.save()
            comp_reference = comp_reference_form.save()

            if int(supplier.profile_completion_status) == 2:
                supplier.profile_completion_status = 3
                supplier.save()
            
            redirect_url = supplier.get_profile_completion_status_redirect_url()
            return HttpResponseRedirect(redirect_url)
        else:
            return render(request, "supplier/company_contacts.html", {})
    else:
        return render(request, "supplier/company_contacts.html", {'supplier': supplier, 'company': company, 'user': user})


def company_contacts_json(request, supplier_id):
    supplier = Supplier.objects.get(id=supplier_id)
    supplier_company= SupplierCompany.objects.get(supplier=supplier)
    comp_contact = CompanyContact.objects.get(company=supplier_company)
    serializer = CompContactSerializer(comp_contact)
    data = json.dumps(serializer.data)
    return HttpResponse(data, content_type='application/json')


def company_references_json(request, supplier_id):
    supplier = Supplier.objects.get(id=supplier_id)
    supplier_company= SupplierCompany.objects.get(supplier=supplier)
    comp_ref = Reference.objects.get(company=supplier_company)
    serializer = CompReferenceSerializer(comp_ref)
    data = json.dumps(serializer.data)
    return HttpResponse(data, content_type='application/json')


def company_licenses(request):
    """
    Controller for company license based on user's request
    :param request:
    :return: HTTPResponse
    """
    
    user = request.user
    supplier = Supplier.objects.get(user=user)
    company = SupplierCompany.objects.get(supplier=supplier)
    try:
        company_license = CompanyLicense.objects.get(company=company)
    except Exception, e:
        company_license = CompanyLicense.objects.create(company=company)

    if request.method == 'POST':
        license_form = CompanyLicenseForm(request.POST, request.FILES, instance=company_license)
        if license_form.is_valid():
            comp_license = license_form.save()
            geo_area = ' '.join(request.POST.getlist('geographical_service_area'))
            comp_license.geographical_service_area = geo_area
            if not request.FILES.get('business_license') and request.POST.get('business_license') == '':
                comp_license.business_license.delete()
            comp_license.save()


            if int(supplier.profile_completion_status) == 3:
                supplier.profile_completion_status = 4
                supplier.save()

            redirect_url = supplier.get_profile_completion_status_redirect_url()
            return HttpResponseRedirect(redirect_url)
    # else:
    #     license_form = CompanyLicenseForm()
    return render(request, "supplier/company_licenses.html", 
        {'supplier' : supplier, 'company' : company })


def company_licenses_json(request, supplier_id):
    supplier = Supplier.objects.get(id=supplier_id)
    supplier_company= SupplierCompany.objects.get(supplier=supplier)
    comp_license = CompanyLicense.objects.get(company=supplier_company)
    serializer = CompanyLicenseSerializer(comp_license)
    data = json.dumps(serializer.data)
    return HttpResponse(data, content_type='application/json')


def company_cert_json(request, supplier_id):
    supplier = Supplier.objects.get(id=supplier_id)
    supplier_company= SupplierCompany.objects.get(supplier=supplier)
    comp_certs = BusinessCertificate.objects.filter(company=supplier_company)
    serializer = CompCertificateSerializer(comp_certs)
    data = json.dumps(serializer.data)
    return HttpResponse(data, content_type='application/json')





def company_products_services(request):
    """
    Controller for company product and service based on user's request
    :param request:
    :return: HTTPResponse
    """
    user = request.user
    supplier = Supplier.objects.get(user=user)
    company = SupplierCompany.objects.get(supplier=supplier)
    try:
        company_product_service = CompanyProductService.objects.get(company=company)    
    except Exception, e:
        company_product_service = CompanyProductService.objects.create(company=company)
    
    if request.method == 'POST':
        product_service_form = CompanyProductServiceForm(request.POST, instance=company_product_service)
        if product_service_form.is_valid():
            company_product_service = product_service_form.save()

            if int(supplier.profile_completion_status) == 4:
                supplier.profile_completion_status = 5
                supplier.save()

            redirect_url = supplier.get_profile_completion_status_redirect_url()
            return HttpResponseRedirect(redirect_url)
    else:
        license_form = CompanyLicenseForm()

    return render(request, "supplier/company_products_services.html", 
        {'supplier' : supplier, 'company' : company})

# @login_required
def company_products_services_json(request, supplier_id):
    """
    API for company product service Model based on supplier ID
    :param request:
    :param supplier_id:
    :return:HttpResponse as JSON
    """
    supplier = Supplier.objects.get(id=supplier_id)
    supplier_company = SupplierCompany.objects.get(supplier=supplier)
    comp_product_service = CompanyProductService.objects.get(company=supplier_company)
    serializer = CompanyProductServiceSerializer(comp_product_service)
    data = json.dumps(serializer.data)
    return HttpResponse(data, content_type='application/json')




def company_certification(request):
    cert_names = CertificateName.objects.all()
    user = request.user
    supplier = Supplier.objects.get(user=user)
    sup_comp, created = SupplierCompany.objects.get_or_create(supplier=supplier)
    if request.method == 'POST':
        certificate_name_list = request.POST.getlist('certificate_name')
        for i in range(0, len(certificate_name_list)):
            cert_name = CertificateName.objects.get(id=certificate_name_list[i])
            comp_cert, created = CompanyCertification.objects.get_or_create(certificate_name=cert_name, company=sup_comp)
            comp_cert.cert_number = request.POST.getlist('cert_number')[i]
            comp_cert.category = request.POST.getlist('category')[i]
            cert_date = request.POST.getlist('certification_date')[i]
            comp_cert.certification_date = datetime.datetime.strptime(cert_date, '%m/%d/%Y')

            expire_date = request.POST.getlist('expire_date')[i]
            comp_cert.expire_date = datetime.datetime.strptime(expire_date, '%m/%d/%Y')

            comp_cert.certificate_duration = request.POST.getlist('certificate_duration')[i]
            try:
                if request.FILES.get('upload_certificate_'+str(i)):
                    comp_cert.upload_certificate = request.FILES.get('upload_certificate_'+str(i))
            except IndexError:
                pass
            
            
            comp_cert.save()
            # f = request.FILES.getlist('upload_certificate')[i]
            # # upload.save()
            # comp_cert.upload_certificate.save(f.name, f)
            # comp_cert.save()
        # supplier.profile_completion_status = 6
        # supplier.save()
        if int(supplier.profile_completion_status) == 5:
                supplier.profile_completion_status = 6
                supplier.save()

        redirect_url = supplier.get_profile_completion_status_redirect_url()
        return HttpResponseRedirect(redirect_url)
    else:
        pass

    return render(request, "supplier/company_certification.html", 
        {'supplier': supplier, 'cert_names': cert_names})

def supplier_submission_status(request):
    user = request.user
    supplier = Supplier.objects.get(user=user)
    selected_buyers = SupplierSelectedBuyers.objects.filter(supplier=supplier)

    return render(request, "supplier/supplier-submission.html", 
        {'buyers': selected_buyers})


# @login_required
def company_certification_json(request, supplier_id):
    """
    API for company certification Model based on supplier ID
    :param request:
    :param supplier_id:
    :return:HttpResponse as JSON
    """
    supplier = Supplier.objects.get(id=supplier_id)
    supplier_company = SupplierCompany.objects.get(supplier=supplier)
    comp_certs = CompanyCertification.objects.filter(company=supplier_company)
    cert_names = CertificateName.objects.all()
    name_list = []
    for cert_name in cert_names:
        name = {}
        name['id'] = cert_name.id
        name['cert_name'] = cert_name.cert_name
        name['description'] = cert_name.description
        certificate = {}
        try:
            certs = CompanyCertification.objects.get(certificate_name=cert_name,company=supplier_company)
            certificate['id'] = certs.id
            certificate['cert_number'] = certs.cert_number
            certificate['category'] = certs.category
            if certs.certification_date:
                certificate['certification_date'] = certs.certification_date.strftime('%m/%d/%Y')
            else:
                certificate['certification_date'] = None

            if certs.expire_date:
                certificate['expire_date'] = certs.expire_date.strftime('%m/%d/%Y')
            else:
                certificate['expire_date'] = None

            certificate['certificate_duration'] = certs.certificate_duration
            if certs.upload_certificate:
                certificate['upload_certificate'] = certs.upload_certificate.name
            else:
                certificate['upload_certificate'] = None    
        except CompanyCertification.DoesNotExist:
            pass
        

        name['company_cert'] = certificate
        name_list.append(name)

    

    data = json.dumps(name_list)

    return HttpResponse(data, content_type='application/json')



def search_code(request, code_type):
    query = request.GET.get('q','')
    if request.GET.get('q'):
        codes = SearchQuerySet().using('code').filter(content=AutoQuery(query)).filter(code_type=code_type).load_all()[:10]
    else:
        codes = SearchQuerySet().using('code').filter(code_type=code_type).load_all()[:10]

    code_list = []
    if codes:
        for code in codes:
            if code != None and code.code_type == code_type:
                code_dict = {}
                code_dict['code'] = code.code
                code_dict['code_type'] = code.code_type
                code_dict['title'] = code.title
                code_dict['common_keywords'] = code.common_keywords
                
                code_list.append(code_dict)

    the_data = json.dumps({
        'results': code_list
    })
    return HttpResponse(the_data, content_type='application/json')


def search(request, code_type):
    return render(request, 'supplier/search_code.html', 
        {'code_type': code_type})


def supplier_capability_statement(request):
    user = request.user
    supplier = Supplier.objects.get(user=user)
    supplier_company = SupplierCompany.objects.get(supplier=supplier)
