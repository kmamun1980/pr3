import datetime
from haystack import indexes
from supplier.models import Buyer, ProductServiceCode


class BuyerIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    company_name = indexes.CharField(model_attr='company_name', boost=2)
    # product_service_category = indexes.CharField(model_attr='product_service_category__category_name', 
    #     indexed=False, null=True)

    def get_model(self):
        return Buyer

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()


class ProductServiceCodeIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    code = indexes.CharField(model_attr='code', boost=2)
    code_type = indexes.CharField(model_attr='code_type', null=True)
    title = indexes.CharField(model_attr='title', null=True)
    common_keywords = indexes.CharField(model_attr='common_keywords', null=True)

    # product_service_category = indexes.CharField(model_attr='product_service_category__category_name', 
    #     indexed=False, null=True)

    def get_model(self):
        return ProductServiceCode

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()



