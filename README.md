paypal: 
sandbox username: mamun1980-buyer@gmail.com
sandbox pass: qweqwe123

how to install locally:

1. clone the project
	$git clone git@github.com:CloudPR/PRDEV.git
2. create a database named 'pr4' and change the username and password to reflect your local in local_settings.py file
3. create virtualenv and active
4. run the requirements file
	$pip install -r requirements.txt
5. run syncdb 
	$python manage.py suncdb
6. run following command to install demo data
	$ python manage.py loaddata supplier.json
	$ python manage.py loaddata subscriber.json
	$ python manage.py loaddata supplier_packages.json