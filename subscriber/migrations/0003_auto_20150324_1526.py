# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('subscriber', '0002_auto_20150311_0913'),
    ]

    operations = [
        migrations.AddField(
            model_name='paymenthistory',
            name='payer_id',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='activationcode',
            name='expire_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 26, 15, 26, 24, 296810), null=True, blank=True),
            preserve_default=True,
        ),
    ]
