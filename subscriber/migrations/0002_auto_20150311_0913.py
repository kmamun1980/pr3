# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('subscriber', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activationcode',
            name='expire_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 3, 13, 9, 13, 24, 951425), null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='activationcode',
            name='status',
            field=models.IntegerField(blank=True, null=True, choices=[(0, b'ACTIVE'), (1, b'USED'), (2, b'INACTIVE'), (3, b'EXPIRED')]),
            preserve_default=True,
        ),
    ]
