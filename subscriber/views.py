from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from subscriber.forms import RegistrationForm, PasswordResetForm
from django.contrib.auth.models import User
from django.contrib import auth, messages
from django.contrib.sites.models import Site
from django.core import serializers
import json
from urllib import parse as urlparse #, parse_qs
from datetime import datetime, timedelta
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from paypalrestsdk import Payment
import logging
import paypalrestsdk
from django.core.mail import send_mail

# Create your views here.
from subscriber.models import *

def supplier_login(request):
    if request.method == "POST":
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        if username == '':
            messages.warning(request, "Please put username")
            return HttpResponseRedirect("/")
        elif password == '':
            messages.warning(request, "Please put password")
            return HttpResponseRedirect("/")
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            if user.is_active and user.is_staff:
                return HttpResponseRedirect("/")
            elif user.is_active and not user.is_staff:
                try:
                    subscriber = Subscription.objects.get(user=user, subscription_status=0)
                    if subscriber.expire_date <= datetime.now(timezone.utc):
                        messages.warning(request, "Your subscription is expired. Please subscribe again.")
                        return HttpResponseRedirect("/subscriber/get-started/")
                except:
                    return HttpResponseRedirect("/subscriber/get-started/")
                if subscriber:
                    try:
                        supplier = Supplier.objects.get(user=user)
                        redirect_url = supplier.get_profile_completion_status_redirect_url()
                    except Supplier.DoesNotExist:
                        return HttpResponseRedirect("/subscriber/get-started/")
                    return HttpResponseRedirect(redirect_url)
                else:
                    return HttpResponseRedirect("/subscriber/get-started/")
            else:
                messages.warning(request, "User %s not active yet" % (username))
                return HttpResponseRedirect("/")
        else:
            messages.warning(request, "User %s does not exits" % (username))
            return HttpResponseRedirect("/")
    else:
        return render(request, "subscriber/supplier-login.html", {})
@login_required
def supplier_logout(request):
    auth.logout(request)
    return HttpResponseRedirect("/")


def registration(request):
    if request.method == "POST":
        
        form = RegistrationForm(request.POST, request=request)
        if form.is_valid():
            form.save()
            
            return HttpResponseRedirect("/")
        else:
            for error in form.errors:
                messages.warning(request, form.errors[error][0])
            return render(request, "subscriber/supplier-registration.html", {})            
    else:

        return render(request, "subscriber/supplier-registration.html", {})


def registration_active(request, activation_code):
    try:
        ac = ActivationCode.objects.get(code=activation_code, status=0, code_for=0)
        ac.status = 1
        ac.save()
        newuser = ac.user
        newuser.is_active = True
        newuser.save()
        return HttpResponseRedirect("/subscriber/"+str(newuser.id)+"/registration/success/")
    except Exception:
        ac = ActivationCode.objects.get(code=activation_code, code_for=0)
        if ac.is_expired:
            messages.add_message(request, messages.INFO, "Activation time is expired")
        elif ac.is_active:
            messages.add_message(request, messages.INFO, "Your account is inactive.")
        return HttpResponseRedirect("/")
    

def registration_success(request, uid):
    requested_user = User.objects.get(id=uid)
    requested_user.backend = 'django.contrib.auth.backends.ModelBackend'
    auth.login(request, requested_user)
    try:
        supplier = Supplier.objects.get(user=requested_user)
        supplier.profile_completion_status = -1
        supplier.save()
        redirect_url = supplier.get_profile_completion_status_redirect_url()
    except Supplier.DoesNotExist:
        return HttpResponseRedirect("/subscriber/get-started/")
    return HttpResponseRedirect(redirect_url)



# @login_required
def get_started(request):
    packages = Package.objects.filter(active=1).order_by("display_order")
    services = Service.objects.all()
    package_plans = PackagePlan.objects.all()
    frequencies = Frequency.objects.all()


    return render(request, "subscriber/get_started.html", 
        {'packages': packages, 'services': services, 'package_plans': package_plans, 
        'frequencies': frequencies})


def get_package_plan(request, ppid):
    pass
    

def get_coupon(request, code):
    try:
        coupon = CouponCode.objects.get(code=code)
        if coupon.expire_date > timezone.now() and coupon.status == '0':
            data = serializers.serialize("json", [coupon,]) 
            struct = json.loads(data)
            data = json.dumps(struct)
            return HttpResponse(data, content_type='application/json')
        elif coupon.expire_date < timezone.now():
            return HttpResponse("Expired")
        elif coupon.status == '1':
            return HttpResponse("Used")
        else:
            return HttpResponse("")
        
    except CouponCode.DoesNotExist:
        return HttpResponse("DoesNotExist")
        

@csrf_exempt
@login_required
def payment_create(request):
    
    user = request.user
    plan_id = request.POST['ppid']
    plan_amount = request.POST['plan_amount']
    net_amount = request.POST['net_amount']
    coupon_code = request.POST['ccid']

    
    plan = PackagePlan.objects.get(pk=plan_id)
    if coupon_code != '0':
        cc = CouponCode.objects.get(pk=coupon_code)
    else:
        cc = None

    try:
        supplier, created = Supplier.objects.get_or_create(user=user)
        supplier.subscription_status = 0
        supplier.save()
    except Exception:
        raise Exception.error
    
    paypalrestsdk.configure({
        "mode": "sandbox", 
        "client_id": "AQuFdtHd3o08g94o_G17lQtzUmGNfAaIjyBDZo05DHWSEJK2DPyX2pgNDWf7HEYLf7li3ZTDm4-ihfPu",
        "client_secret": "EAFbR5ru0JaKuOMidzGAhd6pU6ULt-dC0awcAP13G3y9WTNkxYC5fUb638KI7SbjJoazJntpOZpzUkue" 
    })
    site = request.META['HTTP_HOST']

    payment = paypalrestsdk.Payment({
        "intent": "sale",
        "payer": {
            "payment_method": "paypal"
        },
        "redirect_urls": {
        "return_url": "http://"+ site +"/subscriber/payment/execute",
        "cancel_url": "http://"+ site +"/subscriber/" },
        "transactions": [{
        "item_list": {
        "items": [{
            "name": plan.package.package_name,
            "sku": plan_id,
            "price": net_amount,
            "currency": "USD",
            "quantity": 1 }]},
            "amount": {
            "total": net_amount,
            "currency": "USD" },
            "description": "This is the payment transaction description." 
        }]
    })
    if payment.create():
        print("Payment[%s] created successfully" % (payment.id))
        ph = PaymentHistory(supplier=supplier, plan=plan, plan_amount=plan_amount, net_amount=net_amount)
        ph.payment_id = payment.id
        ph.payment_status = payment.state
        ph.payer_id = payment.create_time
        ph.create_time = payment.create_time
        
        # Redirect the user to given approval url
        for link in payment.links:
            if link.method == "REDIRECT":
                redirect_url = link.href
                # query = parse_qs(urlparse(redirect_url).query)
                query = parse(redirect_url).query
                token = query['token'][0]
                ph.token = token
                ph.save()
        
        return redirect(redirect_url)
    else:
        print("Error while creating payment:")
        print(payment.error)

    return HttpResponseRedirect("/subscriber/payment/error/")

    
@login_required
def payment_execute(request):
    sp = Supplier.objects.get(user=request.user)
    token = request.GET['token']
    payer_id = request.GET['PayerID']
    ph = PaymentHistory.objects.get(token=token, supplier=sp)
    ph.payment_status = 'completed'
    ph.save()

    if ph.coupon:
        cs = CouponSupplier(coupon=ph.coupon, subscriber=sp.user, date_time=datetime.now())
        cs.save()

    subs = Subscription(user=sp.user, package_plan=ph.plan)
    subs.expire_date = datetime.now()+timedelta(days=int(ph.plan.frequency.duration_title))
    subs.plan_amount = ph.plan_amount
    subs.net_amount = ph.net_amount
    subs.subscription_status = 0
    subs.save()

    sp.subscription_status = 1
    sp.profile_completion_status = -1
    sp.save()

    return HttpResponseRedirect("/subscriber/payment/success/")

def payment_success(request):
    return HttpResponseRedirect("/supplier/search-corporation/")

def payment_error(request):
    messages.add_message(request, messages.INFO, "Error in payment please contact admin.")
    return HttpResponseRedirect("/subscriber/get-started/")

def forget_password(request):
    if request.method == 'POST':
        import uuid
        import datetime
        from md5 import md5
        email = request.POST.get("email")
        try:
            user = User.objects.get(email=email)
            activation_code = md5(str(uuid.uuid4())).hexdigest()
            code = ActivationCode.objects.create(code=activation_code)
            code.user = user
            code.creation_date = datetime.datetime.now()
            code.code_for = 1
            code.status = 0
            code.save()
            current_site = Site.objects.get_current()
            HOSTNAME = current_site.domain
            message = 'Click following link'
            url = "http://" + HOSTNAME + "/subscriber/reset-password/" + activation_code + "/"
            message += " <a href="+url+">Click to Reset Password </a>"
            send_mail('Password Reset Link', message, 'benfatola@cloudcustomsolutions.com',
                [email], fail_silently=False)

            return HttpResponseRedirect("/")
        except User.DoesNotExist:
            messages.add_message(request, messages.INFO, "User does not exist with this email.")
            return render(request, 'subscriber/forget_password.html',{})
    else:
        return render(request, 'subscriber/forget_password.html',{})


def reset_password(request, code):
    try:
        ac = ActivationCode.objects.get(code=code, status=0)
        user_id = ac.user.id
    except Exception:
        messages.add_message(request, messages.INFO, 'Could not reset password. Wrong link.')
        return HttpResponseRedirect("/")
    if request.method == 'POST':
        # import pdb; pdb.set_trace()
        new_pass = request.POST.get('password')
        
        user = User.objects.get(id=user_id)
        user.set_password(new_pass)
        user.save()
        ac.status = 1
        ac.save()
        messages.add_message(request, messages.INFO, 'Your password is saved.')
        return HttpResponseRedirect("/")
    else:
        return render(request, "subscriber/reset_password.html", 
            {'user_id': user_id, 'code': code})
        
    